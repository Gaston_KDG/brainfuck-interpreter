import be.gato.brainfuck.Interpreter;
import be.gato.brainfuck.memory.ChainMemory;
import be.gato.brainfuck.memory.SimpleMemory;

public class Program {
    public static void main(String[] args) throws Exception {
        Interpreter interpreter = new Interpreter();
        interpreter.buildInterpreter("helloworld.bf");
        interpreter.runInterpreter();
        interpreter.runInterpreter(new ChainMemory());
        //interpreter.runInterpreter(new SimpleMemory(1));
    }
}
