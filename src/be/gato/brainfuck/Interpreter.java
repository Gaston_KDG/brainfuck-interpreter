package be.gato.brainfuck;

import be.gato.brainfuck.memory.Memory;
import be.gato.brainfuck.memory.SimpleMemory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Scanner;
import java.util.function.Consumer;

public class Interpreter {
    static final Scanner INPUT = new Scanner(System.in);

    private Consumer<Memory> instructions;

    public Interpreter() {
        instructions = men -> {
        };
    }

    public void buildInterpreter(String fileName) throws Exception {
        try (BufferedReader in = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream("/" + fileName)))) {
            boolean eof = false;
            do {
                int op = in.read();
                char ch = (char) op;
                if (op == -1) {
                    eof = true;
                } else {
                    if (Operators.isValid(ch)) {
                        handleOperator(ch, in);
                    }
                }
            } while (!eof);
        }
    }

    public void runInterpreter(Memory memory) {
        instructions.accept(memory);
    }

    public void runInterpreter() {
        instructions.accept(new SimpleMemory());
    }

    private void handleOperator(char ch, Reader in) throws Exception {
        if (ch == '[') {
            instructions = instructions.andThen(buildLoop(in));
        } else {
            instructions = instructions.andThen(Operators.get(ch));
        }
    }

    private Consumer<Memory> buildLoop(Reader in) throws Exception {
        Consumer<Memory> loopInner = buildLoopCode(in);

        return (mem -> {
            while (!mem.isZero()) {
                loopInner.accept(mem);
            }
        });
    }

    private Consumer<Memory> buildLoopCode(Reader in) throws Exception {
        Consumer<Memory> loop = mem -> {
        };

        boolean loopEnded = false;

        while (!loopEnded) {
            int op = in.read();
            char ch = (char) op;
            if (!Operators.isValid(ch)) continue; //skip invalid characters

            if (ch == ']') {
                loopEnded = true;
            } else {
                if (ch == '[') {
                    loop = loop.andThen(buildLoop(in));
                } else {
                    loop = loop.andThen(Operators.get(ch));
                }
            }
        }
        return loop;
    }
}
