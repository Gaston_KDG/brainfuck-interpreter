package be.gato.brainfuck.memory;

public class ChainMemory implements Memory {
    private Block current;

    public ChainMemory() {
        current = new Block();
    }

    @Override
    public void left() {
        current = current.getPrevious();
    }

    @Override
    public void right() {
        current = current.getNext();
    }

    @Override
    public void increment() {
        current.setValue((byte) (current.value + 1));
    }

    @Override
    public void decrement() {
        current.setValue((byte) (current.value - 1));
    }

    @Override
    public byte get() {
        return current.value;
    }

    @Override
    public void put(byte b) {
        current.value = b;
    }

    @Override
    public boolean isZero() {
        return ((int) current.value) == 0;
    }

    private class Block {
        private byte value;
        private Block next;
        private Block previous;

        private Block() {
            next = null;
            previous = null;
        }

        private Block(Block previous, Block next) {
            this.next = next;
            this.previous = previous;
        }

        public byte getValue() {
            return value;
        }

        private void setValue(byte value) {
            this.value = value;
        }

        private Block getNext() {
            if (next == null) {
                next = new Block(this, null);
            }
            return next;
        }

        private Block getPrevious() {
            if (previous == null) {
                previous = new Block(null, this);
            }
            return previous;
        }
    }
}
