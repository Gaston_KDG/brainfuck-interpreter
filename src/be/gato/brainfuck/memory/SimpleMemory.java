package be.gato.brainfuck.memory;

/**
 * Memory to be used for fixed size programs
 * This is the default memory when no other is specified
 */
public final class SimpleMemory implements Memory {
    private byte[] cells;
    private int ptr;

    public SimpleMemory() {
        this(30_000);
    }

    public SimpleMemory(int cap) {
        if (cap < 1) throw new UnsupportedOperationException();
        cells = new byte[cap];
        ptr = 0;
    }


    @Override
    public void left() {
        ptr--;
    }

    @Override
    public void right() {
        ptr++;
    }

    @Override
    public void increment() {
        cells[ptr] = (byte) (cells[ptr] + 1);
    }

    @Override
    public void decrement() {
        cells[ptr] = (byte) (cells[ptr] - 1);
    }

    @Override
    public byte get() {
        return cells[ptr];
    }

    @Override
    public void put(byte b) {
        cells[ptr] = b;
    }

    @Override
    public boolean isZero() {
        return ((int) cells[ptr]) == 0;
    }
}