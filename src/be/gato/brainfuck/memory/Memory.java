package be.gato.brainfuck.memory;

public interface Memory {
    void left();

    void right();

    void increment();

    void decrement();

    byte get();

    void put(byte b);

    boolean isZero();
}
