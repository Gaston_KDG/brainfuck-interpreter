package be.gato.brainfuck;

import be.gato.brainfuck.memory.Memory;

import java.util.function.Consumer;

import static be.gato.brainfuck.Interpreter.INPUT;

class Operators {
    private Operators() {
    }

    public static Consumer<Memory> get(char ch) {
        switch (ch) {
            case '<':
                return Memory::left;
            case '>':
                return Memory::right;
            case '+':
                return Memory::increment;
            case '-':
                return Memory::decrement;
            case '.':
                return mem -> {
                    char out = (char) mem.get();
                    System.out.print(out);
                };
            case ',':
                return mem -> mem.put((byte) INPUT.next().charAt(0));
            default:
                throw new UnsupportedOperationException();
        }
    }

    public static boolean isValid(char ch) {
        switch (ch) {
            case '<':
            case '>':
            case '+':
            case '-':
            case '.':
            case ',':
            case '[':
            case ']':
                return true;
            default:
                return false;
        }
    }
}
